
import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import HotelList from './src/components/HotelList';
import Search from './src/components/Search';


const App= () => {
  

  return (
    <SafeAreaView style={backgroundStyle}>
      <Search/>
      <HotelList/>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  
});

export default App;
